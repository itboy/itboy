$(document).ready(function() {
    var page_url = location.href;
    var entries = $("#main .skinArticleHeader2 h1 a");
    var classname = ".articleDetailArea";

    entries.each(function(i){ 
        var url = $(this).attr('href');
        title = $(this).text();

        var hatena_btn = '<a href="http://b.hatena.ne.jp/entry/' + url + '" class="hatena-bookmark-button" data-hatena-bookmark-title="' + title + '" data-hatena-bookmark-layout="standard" title="このエントリーをはてなブックマークに追加"><img src="http://b.st-hatena.com/images/entry-button/button-only.gif" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a>';

        var twitter_btn = '<a href="https://twitter.com/share" class="twitter-share-button" data-via="it_boy" data-lang="ja" data-text="' + title + ' | A Day In The Boy&#39;s Life" data-url="' + url + '">ツイート</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>';

        var gplus_btn = '<div class="g-plusone" data-size="medium" data-href=' + url + '></div>';

        $(classname).eq(i).append('&nbsp;&nbsp;&nbsp;' + hatena_btn + '&nbsp;' + twitter_btn + '&nbsp;' + gplus_btn);
    });

    if (entries.length > 0) {
        $.getScript("https://b.st-hatena.com/js/bookmark_button.js");
        $.getScript("https://apis.google.com/js/platform.js", function(){
            {lang: 'ja'}
        });
        $.getScript("https://platform.twitter.com/widgets.js");

        var shl_url = "http://alexgorbatchev.com/pub/sh/current/";
        $("head").append('<link rel="stylesheet" href="' + shl_url + 'styles/shThemeRDark.css" type="text/css" />');
        $("head").append('<link rel="stylesheet" href="' + shl_url + 'styles/shCore.css" type="text/css" />');

        $.getScript(shl_url + "scripts/shCore.js", function() {
            $.getScript(shl_url + "scripts/shAutoloader.js", function() {
                function path() {
                  var args = arguments,
                  result = [];

                  for (var i = 0; i < args.length; i++)
                    result.push(args[i].replace('@', shl_url + 'scripts/'));
                    return result
                };

                SyntaxHighlighter.autoloader.apply(null, path(
                'applescript            @shBrushAppleScript.js',
                'actionscript3 as3      @shBrushAS3.js',
                'bash shell             @shBrushBash.js',
                'coldfusion cf          @shBrushColdFusion.js',
                'cpp c                  @shBrushCpp.js',
                'c# c-sharp csharp      @shBrushCSharp.js',
                'css                    @shBrushCss.js',
                'delphi pascal          @shBrushDelphi.js',
                'diff patch pas         @shBrushDiff.js',
                'erl erlang             @shBrushErlang.js',
                'groovy                 @shBrushGroovy.js',
                'java                   @shBrushJava.js',
                'jfx javafx             @shBrushJavaFX.js',
                'js jscript javascript  @shBrushJScript.js',
                'perl pl                @shBrushPerl.js',
                'php                    @shBrushPhp.js',
                'text plain             @shBrushPlain.js',
                'py python              @shBrushPython.js',
                'ruby rails ror rb      @shBrushRuby.js',
                'sass scss              @shBrushSass.js',
                'scala                  @shBrushScala.js',
                'sql                    @shBrushSql.js',
                'xml xhtml xslt html    @shBrushXml.js'
                ));
                SyntaxHighlighter.all();
            });
        });
    }
});
