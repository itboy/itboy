$(document).ready(function() {
  $("head").append('<link rel="stylesheet" href="http://alexgorbatchev.com/pub/sh/2.1.382/styles/shThemeDefault.css" type="text/css" />');
  $("head").append('<link rel="stylesheet" href="http://alexgorbatchev.com/pub/sh/2.1.382/styles/shCore.css" type="text/css" />');

  $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shCore.js", function() {

    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushCss.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushJScript.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushPhp.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushRuby.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushSql.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushVb.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushXml.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushPerl.js");
    $.getScript("http://alexgorbatchev.com/pub/sh/2.1.382/scripts/shBrushPlain.js");

    SyntaxHighlighter.config.bloggerMode = true;
    SyntaxHighlighter.config.clipboardSwf = 'http://alexgorbatchev.com/pub/sh/2.1.382/scripts/clipboard.swf';
    SyntaxHighlighter.all();
  });

  $('<meta http-equiv="content-language" content="ja" />').remove();

});
